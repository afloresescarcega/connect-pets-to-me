import json
import unicodedata

import requests

import database

# Request URLs
findPetsURL = "http://api.petfinder.com/pet.find"
getPetURL = "http://api.petfinder.com/pet.get"
getShelterURL = "http://api.petfinder.com/shelter.get"
getRandomPetURL = "http://api.petfinder.com/pet.getRandom"
yahooSearchBusinessURL = "http://api.yelp.com/v3/businesses/search"
yelpSearchBusinessByID = "https://api.yelp.com/v3/businesses/"

googleMapGetCoords = 'https://maps.googleapis.com/maps/api/staticmap?center='
googleMapOptions = '&zoom=12&size=250x250&scale=2&markers=color:blue%7Csize:tiny%7Clabel:S%7C'
googleMapKey = '&key=AIzaSyAVelkQ8ysqaPPt5UHxv_IOHuaYFBpbDiM'

# Base arguments for requests
baseParams = {'format': 'json',
              'key': 'd417863a4e46a7ef24e5b546c2b0152d',
              }

baseYahooParams = {
    'term': 'vet'
}

yahooAuth = {'Authorization':
             'Bearer Cod1YPuV7dW1iemFKzKuiMXj5-QsI-YypeDQJsakzCNIMkKTpaxAsDN6stIJC69NLdRzoupA5wm_kblhT0_wDLAopKyXVmcbX57pLjGXIYQhE1Ud3tx95ec0BOQ8W3Yx'}


petList = []
vList = []


def num(s):
    """
    Tries to convert any number into a float
    """
    try:
        return float(s)
    except ValueError:
        file = open('error.txt', 'w')
        file.write('getPetsUtil.num() failed try clause')
        file.flush()
        assert False


def unicode(s):
    """
    Tries to convert any string into unicode
    """
    if isinstance(s, str):
        try:
            return s.decode('utf-8')
        except AttributeError:
            return s
    return None


def stringify(s):
    """
    Tries to convert any string into ASCII
    """
    try:
        return unicodedata.normalize('NFKD', s).encode('ascii', 'ignore')
    except Exception:
        return s


def parseVets(vetList, parsed_json):
    """
    Helper method for parsing the JSON returned from requesting vets with
    the  Yelp API.
    """
    businesses = parsed_json['businesses']
    for biz in businesses:
        try:
            getOneBizReq = yelpSearchBusinessByID + biz['alias']
            biz_entity = json.loads(
                requests.get(
                    getOneBizReq,
                    headers=yahooAuth).text)

            # Error checks for photos in the returned JSON.
            if 'photos' not in biz_entity:
                assert None
            if len(biz_entity['photos']) is 0:
                photos = None
            else:
                photos = biz_entity['photos']

            vetList.append(database.Vet(
                id=unicode(biz['id']),
                name=biz['name'],
                image_url=biz['image_url'],
                address1=biz['location']['address1'],
                address2=biz['location']['address2'],
                address3=biz['location']['address3'],
                city=biz['location']['city'],
                state=biz['location']['state'],
                country=biz['location']['country'],
                zip_code=biz['location']['zip_code'],
                phone=biz['phone'],
                display_phone=biz['display_phone'],
                latitude=num(biz['coordinates']['latitude']),
                longitude=num(biz['coordinates']['longitude']),
                rating=num(biz['rating']),
                review_count=num(biz['review_count']),
                hours=biz_entity['hours'],
                website_url=biz_entity['url'],
                all_images=photos)
            )
        except KeyError as e:
            print('Error reading vet')


def getVetsByLocation(location):
    """
    Gets a list of vets from Yelp API from the location (zipcode) specified.
    """
    vetList = []

    baseYahooParams['location'] = location

    try:
        r = requests.get(
            yahooSearchBusinessURL,
            params=baseYahooParams,
            headers=yahooAuth)
        parsed_json = json.loads(r.text)

        parseVets(vetList, parsed_json)

        return vetList

    except Exception as e:
        raise


def getRandomPetFromPetFinder():
    """
    Gets a completely random pet from PetFinder API.
    Can only get one at a time.
    """
    try:
        r = requests.get(getRandomPetURL, params=baseParams)
        parsed_json = json.loads(r.text)

        petId = parsed_json['petfinder']['petIds']['id']['$t']

        return getPetByIdFromPetFinder(petId)

    except Exception as e:
        raise


def parsePet(petD):
    """
    Helper method to parse the JSON returned after getting a pet from
    PetFinder API
    """
    petId = petD['id']['$t']
    petType = petD['animal']['$t']
    petName = petD['name']['$t']
    petBreeds = petD['breeds']['breed']

    # Some pets will have more than one breed (or no breed listed).
    # The following code covers those cases.
    if len(petBreeds) == 0:
        petBreed = 'No Breed Listed'
    elif len(petBreeds) == 1:
        petBreed = petD['breeds']['breed']['$t']
    else:
        petBreed = ''
        for b in petBreeds:
            petBreed += b.get('$t') + '/'
        petBreed = petBreed[:-1]

    petAge = petD['age']['$t']
    petSex = petD['sex']['$t']
    petSize = petD['size']['$t']

    """
    Photos come back in a list of up to 15 photos.  Each has a size and there
    may be more than 1 of any given size.  The feature photo is size
    'fpm' - featured pet module (max 95 pixels wide) - for not we're grabbing
    that 1.
    """
    if not petD['media']:
        petPhoto = ""
        allPhotos = []
    else:
        allPhotos = []
        petPhotos = petD['media']['photos']['photo']
        for photo in petPhotos:
            if photo['@size'] == 'x':
                petPhoto = photo['$t']
            allPhotos.append(photo['$t'])
    if not petD['description']:
        petDes = ""
    else:
        petDes = petD['description']['$t']
    petShelterId = petD['shelterId']['$t']

    return database.Pet(
        id=unicode(petId),
        name=stringify(petName),
        breed=stringify(petBreed),
        sex=stringify(petSex),
        animal=stringify(petType),
        age=stringify(petAge),
        size=stringify(petSize),
        description=stringify(petDes),
        image_url=unicode(petPhoto),
        all_images=allPhotos,
        shelter_id=unicode(petShelterId)
    )


def getPetsFromPetFinder(animalType, count, location):
    """
    Gets a list of pets from PetFinder API after specifying an animal,
    count, and location.
    """
    petL = []
    try:
        baseParams['count'] = count
        baseParams['animal'] = animalType
        baseParams['location'] = location
        r = requests.get(findPetsURL, params=baseParams)
        parsed_json = json.loads(r.text)

        pets = parsed_json['petfinder']['pets']['pet']

        for petD in pets:
            petL.append(parsePet(petD))

        return petL

    except Exception as e:
        raise e


def getPetsNonSpecific(count, location):
    """
    Gets a list of pets from PetFinder API after specifying only count
    and location.
    """
    petL = []
    try:
        baseParams['count'] = count
        baseParams['location'] = location
        r = requests.get(findPetsURL, params=baseParams)
        parsed_json = json.loads(r.text)

        pets = parsed_json['petfinder']['pets']['pet']

        for petD in pets:
            petL.append(parsePet(petD))

        return petL

    except Exception as e:
        raise e


def getShelterByIdFromPetFinder(shelterId):
    """
    Gets a shelter from PetFinder by using the ID thats inside the
    each pet's JSON.
    """
    requestParams = baseParams

    try:
        requestParams['id'] = shelterId
        r = requests.get(getShelterURL, requestParams)
        parsed_json = json.loads(r.text)

        if 'shelter' not in parsed_json['petfinder']:
            return None
        else:
            shelter = parsed_json['petfinder']['shelter']
        longitude = shelter['longitude']['$t']
        latitude = shelter['latitude']['$t']
        name = shelter['name']['$t']
        if not shelter['phone']:
            phone = None
        else:
            phone = shelter['phone']['$t']
        email = shelter['email']['$t']

        if not shelter['address1']:
            address1 = None
        else:
            address1 = shelter['address1']['$t']
        if not shelter['address2']:
            address2 = None
        else:
            address2 = shelter['address2']['$t']
        city = shelter['city']['$t']
        state = shelter['state']['$t']
        zipCode = shelter['zip']['$t']

        # Uses the Google Maps API to find map images of the specified
        # Longitude and latitude.
        coords = str(latitude) + ',' + str(longitude)
        googleMapImgUrl = googleMapGetCoords + coords + \
            googleMapOptions + coords + googleMapKey

        return database.Shelter(
            id=unicode(shelterId),
            name=name,
            state=state,
            city=city,
            address1=address1,
            address2=address2,
            zip=zipCode,
            phone=phone,
            longitude=num(longitude),
            latitude=num(latitude),
            email=email,
            img_url=googleMapImgUrl
        )
    except Exception as e:
        return None


def getPetByIdFromPetFinder(petId):
    """
    Gets a specific pet (by ID) from PetFinder API.
    """
    requestParams = baseParams

    try:
        requestParams['id'] = petId
        r = requests.get(getPetURL, requestParams)
        parsed_json = json.loads(r.text)

        if not parsed_json['petfinder']['pet']:
            print("Error looking for petID: ", petId)
        else:
            petD = parsed_json['petfinder']['pet']

        return parsePet(petD)

    except Exception as e:
        None
