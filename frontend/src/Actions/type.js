export const FETCH_PET_PAGE = 'FETCH_PET_PAGE';
export const FETCH_VET_PAGE = 'FETCH_VET_PAGE';
export const FETCH_SHELTER_PAGE = 'FETCH_SHELTER_PAGE';
export const NEW_DATA = 'NEW_DATA';

export const FETCH_PET_ENTRY = 'FETCH_PET_ENTRY';
export const FETCH_VET_ENTRY = 'FETCH_VET_ENTRY';
export const FETCH_SHELTER_ENTRY = 'FETCH_SHELTER_ENTRY';

export const HOST = 'localhost';
// export const HOST = '34.239.124.91';

