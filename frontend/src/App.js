import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './Store';
import Navbar from './Components/Navbar';
import Footer from './Components/Footer';
import PetList from './Pages/ListingPage/PetList/petList';
import About from './Pages/About';
import vetList from './Pages/ListingPage/VetList/vetList';
import shelterList from './Pages/ListingPage/ShelterList/shelterList';
import Carousel from './Pages/Home/Carousel';
import PetSearch from './Pages/Search/PetSearch';
import VetSearch from './Pages/Search/VetSearch';
import ShelterSearch from './Pages/Search/ShelterSearch';
import PetEntity from './Pages/Entities/PetEntity';
import ShelterEntity from './Pages/Entities/ShelterEntity';
import VetEntity from './Pages/Entities/VetEntity';
import NotFound from './Pages/NotFound';
import ScrollToTop from './Components/ScrollToTop';
import SearchResultsPage from './Pages/Search/SearchResultsPage';
import D3Map from './Pages/D3Map/D3Map';

class App extends Component {
  componentWillMount() {
    document.title = 'Connect Pets To Me';
  }
  render() {
    return (
      <Provider store={store}>
        <Router>
          <ScrollToTop>
            <div className="App">
              <Navbar/>
              <Switch>
                <Route exact path='/' component={Carousel}/>
                <Route path='/About' component={About}/>
                <Route path='/D3Map' component={D3Map}/>
                <Route path='/FindPets/:pageNum' component={PetList}/>
                <Route path='/FindVets/:pageNum' component={vetList}/>
                <Route path='/FindShelters/:pageNum' component={shelterList}/>
                <Route path='/Pets/PetEntity/:petId' component={PetEntity}/>
                <Route path='/Shelters/ShelterEntity/:shelterId' component={ShelterEntity}/>
                <Route path='/Vets/VetEntity/:vetId' component={VetEntity}/>
                <Route path='/Search/:pageNum'component={SearchResultsPage} />
                <Route path='/SearchPets/:pageNum' component={PetSearch} />
                <Route path='/SearchVets/:pageNum' component={VetSearch} />
                <Route path='/SearchShelters/:pageNum' component={ShelterSearch} />
                <Route path="*" component={NotFound}/>
              </Switch>
              <Footer/>
            </div>
          </ScrollToTop>
        </Router>
      </Provider>
    );
  }
}

export default App;
