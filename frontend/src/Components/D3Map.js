import React, { Component } from 'react';
import {drawMap} from './uStates';
import * as d3 from 'd3';
import '../CSS/D3Map.css'

function tooltipHtml(n, d){ /* function to create html content string in tooltip div. */
  let table = "<h4>"+n+"</h4><table>"
  for (var key in d){
    if (key === 'color')
      continue;
    table = table.concat("<tr><td>", key, "</td><td>", d[key], "</td></tr>")
  }
  return table.concat("</table>");
}

export default class D3Map extends Component {
  constructor(props) {
    super(props);
    this.state= {
      sampleData: {}
    }
  }

  componentDidMount(){
    

    d3.select("div#mapd3".concat(this.props.type))
      .append("svg")
      .attr("id", "statesvg".concat(this.props.type))
      //responsive SVG needs these 2 attributes and no width and height attr
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 1260 800")
      .style('transform', 'translate(10%, 10%)')

      d3.select("div#mapd3".concat(this.props.type))
      .append("div")
      .attr("id", "tooltip".concat(this.props.type));
  }

  componentDidUpdate(){
    var tmp = this.props.info.stats;
    var color = this.props.color;
    if (color === undefined)
      color = ["#fff2e6", "#e67300"]

    if (tmp !== undefined){
      for (let state in this.props.info.stats){

        // here assign num btw 0 - 1 for color interpolate("#ffffcc", "#800026")
        // normalize number of total pets for each state by sigmoid function
        let total = tmp[state].total;
        let median = this.props.info.median;
        let sigmoid = 1 / (1 + Math.exp(-1 * (total - median) / median ) );
        tmp[state]['color'] = d3.interpolate(color[0], color[1])(sigmoid); 
      }
      drawMap("#statesvg".concat(this.props.type), tmp, tooltipHtml, "#tooltip".concat(this.props.type), this.props.type);
    }
  }

  render(){
    return (
        <div id={"mapd3".concat(this.props.type)}></div>
    )
  }
}
