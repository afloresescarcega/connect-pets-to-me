import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {DropItem} from './DropItem';
import queryString from 'query-string';
import {
  mapToDisplay,
  mapSizeToDisplay
} from './../Assets/StaticData'
/*
extra filters only displayed on pet page due to lack of space
*/
export default class FilterMenu extends Component {
  render() {
    this.queryObject = queryString.parse(this.props.query);
    let reset = queryString.parse(this.props.query);
    delete(reset[this.props.type]);
    let upperCaseType = this.props.type.charAt(0).toUpperCase() + this.props.type.slice(1);
    let displayName = "";
    switch (this.props.type) {
      case 'sex': displayName = mapToDisplay[this.queryObject[this.props.type]]; break;
      case 'size': displayName = mapSizeToDisplay[this.queryObject[this.props.type]]; break;
      default: displayName = this.queryObject[this.props.type];
    }
    return (
      <div className="dropdown">
        <button className="btn dropdown-toggle" style={this.style} type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {this.queryObject[this.props.type] === undefined ?
          "Filter by ".concat(upperCaseType) :
          upperCaseType.concat(': ', displayName)
        }
        </button>
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <Link className="dropdown-item" to={this.props.domain.concat('1?', queryString.stringify(reset))}>Reset Filter</Link>
          <DropItem domain={this.props.domain} type={this.props.type} queries={this.props.query}/>
        </div>
      </div>
    )
  }
}