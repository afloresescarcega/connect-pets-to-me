import React, { Component } from 'react';
import RatingStar from './../../../Components/RatingStar';
import { Link } from 'react-router-dom';
import '../../../CSS/VetItems.css'

export default class VetItems extends Component {
  render() {
    return (
    <div className="col-md-4 d-flex">
        <div className="card mb-4 box-shadow">
          <div className="vetimg">
            <Link to={"/Vets/VetEntity/".concat(this.props.vetData.id)}><img className="card-img-top" src={this.props.vetData.image_url} onError={(e)=>{e.target.src="https://www.scandichotels.se/Static/img/placeholders/image-placeholder_3x2.svg"}} alt=""/></Link>
          </div>
          <div className="card-body">
          <Link to={"/Vets/VetEntity/".concat(this.props.vetData.id)}>{this.props.vetData.name}</Link>
            <RatingStar rating= {this.props.vetData.rating}/>
            <br />
            <div className="d-flex justify-content-between align-items-center">
              <h6 className="badgeList">
                <span className="badge badge-pill badge-light">{this.props.vetData.city}</span>
                <span className="badge badge-pill badge-light">{this.props.vetData.display_phone}</span>
              </h6>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
