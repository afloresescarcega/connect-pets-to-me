import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import './../CSS/Navbar.css';
import queryString from 'query-string';
import { HOST } from '../Actions/type';

class Navbar extends Component{

  constructor(props){
    super(props)
      this.state = {
        q: ""
      }
      this.searchUpdate = this.searchUpdate.bind(this)
      this.style = {
      backgroundColor: "#a1887f",
    }
  }

  clearSearch() {
    this.setState({q: ""})
  }

  /*
  routes user and clears search text
  */
  submitSearch(event) {
    if (event.key === 'Enter'){
      this.props.history.push('/Search/1?'.concat(queryString.stringify(this.state)));
      this.clearSearch();
    }
  }

  searchUpdate(event){
    this.setState({q: event.target.value});
  }

  render(){
    let domain = this.props.location.pathname.split("/")[1];
    return (
      <nav className="navbar navbar-expand-lg navbar-dark" style={this.style}>
        <div className="container">
          <Link className="navbar-brand" to="/">Connect Pets To Me</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarsExample07">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Visualizations</a>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <Link className="dropdown-item" to="/D3Map">Stats Map</Link>
                  <a className="dropdown-item" href={"http://"+HOST+":5000/d3bubble"}>Stats Bubble</a>
                </div>
              </li>
              <li className={"nav-item".concat(domain === 'FindPets' ? ' activePage' : "")}>
                <Link className="nav-link" to="/FindPets/1">Find Pets</Link>
              </li>
              <li className={"nav-item".concat(domain === 'FindVets' ? ' activePage' : "")}>
                <Link className="nav-link" to="/FindVets/1">Find Vets</Link>
              </li>
              <li className={"nav-item".concat(domain === 'FindShelters' ? ' activePage' : "")}>
                <Link className="nav-link" to="/FindShelters/1">Find Shelters</Link>
              </li>
              <li className={"nav-item".concat(domain === 'About' ? ' activePage' : "")}>
                <Link id="about"  className="nav-link" to="/About">About Us</Link>
              </li>
            </ul>
            {domain === 'Search' ? null :
            <form className="form-inline my-2 my-lg-0">
              <input className="form-control" type="search" value={this.state.q} placeholder="Search Website" onChange={this.searchUpdate.bind(this)} id="search-input" onKeyPress={this.submitSearch.bind(this)}/>
              <Link to={"/Search/1?".concat(queryString.stringify(this.state))}>
                <button className="btn" type="submit" onClick={this.searchUpdate}>Search</button>
              </Link>
            </form>
            }
          </div>
        </div>
      </nav>
    )
  }
}

export default withRouter(Navbar)