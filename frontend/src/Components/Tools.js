import React, { Component } from 'react';
import { tools_json } from './../Assets/StaticData';

export default class Tools extends Component {
  render() {
    return tools_json.map(
      tool => (                  
        <div className="col-md-3 d-flex align-items-stretch" key={tool.name}>
          <div className="card mb-3 box-shadow">
            <div className="card-body">
              <h5 className="card-title">{tool.name}</h5>
              <p className="card-text">{tool.description}</p>
            </div> 
          </div>
        </div>
      )
    )
  }
}
