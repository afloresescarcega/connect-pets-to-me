import React, {Component} from 'react';
import './../../CSS/Carousel.css';
import { HOST } from '../../Actions/type';
import D3Map from '../../Components/D3Map'

class D3Carousel extends Component {

    constructor(props) {
        super(props);
        this.state= {
          pet_stats: {},
          vet_stats: {},
          shelter_stats: {}
        }
      }
    
    componentWillMount() {
    fetch('http://'+HOST+':5000/api/stats/pet')
        .then((resp) => resp.json()) // Transform the data into json
        .then(data => this.setState({pet_stats: data}))

    fetch('http://'+HOST+':5000/api/stats/vet')
        .then((resp) => resp.json()) // Transform the data into json
        .then(data => this.setState({vet_stats: data}))

    fetch('http://'+HOST+':5000/api/stats/shelter')
        .then((resp) => resp.json()) // Transform the data into json
        .then(data => this.setState({shelter_stats: data}))
    }

    render(){

    let image = {
        background: 'none',
    }

    return(
    <div>
        <div id="frontPageSlider" className="carousel slide" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#frontPageSlider" data-slide-to="0" className="active"></li>
            <li data-target="#frontPageSlider" data-slide-to="1"></li>
            <li data-target="#frontPageSlider" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner" role="listbox">

            <div className="carousel-item active" >
              <div className="jumbotron p-3 p-md-5 text-white rounded" style={image}>
                  <div className ="jumbotron_wrapper container">
                    <h1><span id="d3text">Pet Stats Map</span></h1>
                    <D3Map info={this.state.pet_stats} type="Pet" />
                    <h3><span id="d3text">Click any state to find pets to adopt.</span></h3>
                  </div>
              </div>
            </div>
            <div className="carousel-item" >
              <div className="jumbotron p-3 p-md-5 text-white rounded" style={image}>
                  <div className ="jumbotron_wrapper container">
                    <h1><span id="d3text">Vet Rating Map</span></h1>
                    <D3Map info={this.state.vet_stats} type="Vet" color={["#e6f7ff", "#0077b3"]}/>
                    <h3><span id="d3text">Click any state to find a vet.</span></h3>
                  </div>
              </div>
            </div>
            <div className="carousel-item" >
              <div className="jumbotron p-3 p-md-5 text-white rounded" style={image}>
                  <div className ="jumbotron_wrapper container">
                    <h1><span id="d3text">Shelter Stats Map</span></h1>
                    <D3Map info={this.state.shelter_stats} type="Shelter" color={["#ffe6e6", "#cc0000"]}/>
                    <h3><span id="d3text">Click any state to find an animal shelter.</span></h3>
                  </div>
              </div>
            </div>
          </div>
          <a className="carousel-control-prev" href="#frontPageSlider" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#frontPageSlider" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div> 
      </div>

    )
  }
}

export default D3Carousel;
