import React, { Component } from 'react'

export default class BreedInfo extends Component {
  render() {
    return (
        <div>
            <h4 className="text-secondary">{this.props.breed}</h4>
            <p className="text-primary">Temperament</p>
            <p className="text-secondary">{this.props.temperament}</p>

            <p className="text-primary">Detail</p>
            <p className="text-secondary">{this.props.detail}</p>
        </div>
    )
  }
}
