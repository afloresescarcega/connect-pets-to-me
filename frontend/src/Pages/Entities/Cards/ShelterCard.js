import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import InteractiveMap from '../../../Components/InteractiveMap';

export default class ShelterCard extends Component {
  render() {
    // Google Map
    return (
      
        <div className="col-md-6">
          <div className="card flex-md-row mb-4 box-shadow h-md-250">
              <div className="card-body d-flex flex-column align-items-start">
                <strong className="d-inline-block mb-2 text-success">Shelter</strong>
                <h5 className="mb-0">
                    <Link className="text-dark" to={"/Shelters/ShelterEntity/".concat(this.props.info.id)}>{this.props.info.name}</Link>
                </h5>
                <p className="card-text mb-auto">{this.props.info.city} {this.props.info.state}</p>
                <p className="card-text mb-auto">Contact: {this.props.info.phone}</p>

                <Link className="text-dark" to={"/Shelters/ShelterEntity/".concat(this.props.info.id)}><p className="text-primary">More Detail</p></Link>
              </div>
              <div className="card-img-right flex-auto d-none d-lg-block">
                {this.props.info.name !== undefined ?
                  <InteractiveMap latitude={this.props.info.latitude} longitude={this.props.info.longitude} name={this.props.info.name} size={123}/>
                : null} 
              </div>
          </div>
        </div>
        
    )
  }
}
