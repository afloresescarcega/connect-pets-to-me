import React, { Component } from 'react'
import FirstItem from './FirstItem';
import RestItem from './RestItem';
import no_img from '../../../Assets/noimg.jpg'

// petInfo has all images, so get 500px large
// images and return a list
function getLargeImages(s){
    let img_list = [];
    let n = -7;
    let s_i = 0;
    while(true){
        s = s.substring(n + 7);
        n = s.indexOf("&-x.");

        if (n === -1)
            break;
        s_i = s.substring(0, n).lastIndexOf('http');
        img_list.push(s.substring(s_i, n + 7));
    }
    return img_list;
}

export default class Carousel extends Component {
  render() {
    
    let img_list = getLargeImages(String(this.props.info.all_images));
    if (img_list.length === 0)
        img_list.push(`${no_img}`)

    // arrow and bottom indicators
    let indicators = []
    let arrow = []
    console.log(img_list.length)
    for (let i = 0; i < img_list.length; i++){
        if (img_list.length === 1)
            break;
        if (i === 0)
            indicators.push(<li data-target="#carousel-example-1z" data-slide-to={i} className="active" key={i}></li>)
        else
            indicators.push(<li data-target="#carousel-example-1z" data-slide-to={i} key={i}></li>)

        if (i === 1){
            arrow.push(<a className="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev" key={i}>
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>)
            arrow.push(<a className="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next" key={i+1}>
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>)
        }
    }

    // carousel items
    let carousels = [];
    let b_first = true;
    for (let img of img_list){
        if (b_first){
            carousels.push(
                <FirstItem image={img} info={this.props.info} key={img}/>
            );
            b_first = false;
        }else{
            carousels.push(
                <RestItem image={img} info={this.props.info} key={img}/>
            );
        }
    }

    return (
        <div id="carousel-example-1z" className="carousel slide carousel-fade" data-ride="carousel">
            <ol className="carousel-indicators">
                {indicators}
            </ol>
            <div className="carousel-inner" role="listbox">
                {carousels}
            </div>
            {arrow}
        </div>
    )
  }
}
