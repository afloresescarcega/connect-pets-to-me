import React, { Component } from 'react'

export default class RestItem extends Component {
  render() {
    // jumbotron design
    let image = {
        background: 'linear-gradient(to right, #020202 0%,#020202 28%,  transparent 70%, transparent), url('+this.props.image+') no-repeat center',
        backgroundSize: 'cover, auto 250px'
    };
    return (
        <div className="carousel-item">
            <div className="jumbotron p-3 p-md-5 text-white rounded bg-dark" style={image}>
                <div className ="jumbotron_wrapper container">
                    <div className="col-md-6 px-0"> 
                        <h1 className="display-4 font-italic text-white">{this.props.info.name}</h1>
                        <p className="lead my-3 text-white">{this.props.info.breed}</p>
                        <p className="lead my-3 text-white">Age: {this.props.info.age}</p>
                        <p className="lead my-3 text-white">{this.props.info.city}</p>
                    </div>
                </div>
            </div>
            {/* <div className="jumbotron p-3 p-md-5 text-white rounded bg-dark" style={image}>
            <div className="container">
                  
                  <img class="image_setting d-block w-80" src={this.props.image} alt="Third slide"/>
              
          </div>
            </div> */}
            
            
        </div>
    )
  }
}
