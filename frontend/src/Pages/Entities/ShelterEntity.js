import React, { Component } from 'react';
import './../../CSS/ShelterEntity.css';
import { connect } from 'react-redux';
import { fetchShelterEntity } from './../../Actions/dataModelAction';
import InteractiveMap from './../../Components/InteractiveMap';
import ImageCard from './../../Components/ImageCard';
import Share from './../../Components/Share';
import {mapToDisplay} from './../../Assets/StaticData';

// import Card from './Card';

export class ShelterEntity extends Component{

    componentWillMount(){
        this.props.fetchShelterEntity(this.props.match.params.shelterId);
    }
    /* Populate side bar and bottom bar with nearby pets and vets                                
    */
    render(){
        let info = this.props.shelterInfo;
        let hours = []
        for(let hour in info.hours){
            hours.push(<li key={hour}>{info.hours[hour]}</li>);
        }
        const close_vets = [];
        if (info.close_vets !== undefined)
            for (var i = 0; i < Math.min(2, info.close_vets.length); i++) {
                close_vets.push(<ImageCard id={info.close_vets[i]} type={'vet'} key={info.close_vets[i]}/>);
            }
        const close_pets = [];
        if (info.pets !== undefined)
            for (i = 0; i < Math.min(4, info.pets.length); i++) {
                close_pets.push(
                    <div className="col-md-3 d-flex align-items-stretch" key={info.pets[i]}>
                        <ImageCard id={info.pets[i]} type={'pet'}/>
                    </div>
                );
            }
        /*
        Display main content
        */
        return (
            <div className="shelterEntity listingPage">
                <main role="main" className="container">
                    <div className="row">
                        <div className="col-md-8 blog-main">
                            <div className="row mb-2">
                                <div className="col-md-12">
                                <br />
                                <br />
                                    <div className="card flex-md-row mb-4 box-shadow">
                                        <div className="card-body d-flex flex-column align-items-start">
                                            <h2 className="d-inline-block mb-2 text-black"><b>{info.name}</b></h2>
                                            {info.address1 !== null ? <div className="mb-1 text-black"><b>Address:</b> {info.address1}</div> : null}
                                            <div className="mb-1 text-black"><b>City:</b> {info.city}</div>
                                            <div className="mb-1 text-black"><b>State:</b> {mapToDisplay[info.state]}</div>
                                            {info.phone !== null ? <div className="mb-1 text-black"><b>Phone:</b> {info.phone}</div> : null}
                                            {info.email !== null ? <div className="mb-1 text-black"><b>Email:</b> {info.email}</div> : null}
                                            <br/>
                                            {info.name !== undefined ?
                                            <InteractiveMap latitude={this.props.shelterInfo.latitude} longitude={this.props.shelterInfo.longitude} name={info.name} />
                                            : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <aside className="col-md-4 blog-sidebar">
                            <Share type='shelter' id={this.props.match.params.shelterId} />
                                <br/>
                            <h3 className="d-inline-block mb-2 text-black">Nearby Vets: </h3>
                            {close_vets}
                        </aside>

                    </div>
                    <div className="container">
                        <h3 className="d-inline-block mb-2 text-black">Pets at this Shelter: </h3>
                        <div className="row">
                            {close_pets}
                        </div>
                    </div>
                </main>

            </div>
        );
    }
}
const mapStateToProps = state => ({
    shelterInfo: state.shelterInfo.items
})

export default connect(mapStateToProps, {fetchShelterEntity})(ShelterEntity)
