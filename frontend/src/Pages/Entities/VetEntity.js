import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchVetEntity } from './../../Actions/dataModelAction';
import RatingStar from './../../Components/RatingStar';
import VetPetLink from './../../Components/VetPetLink';
import './../../CSS/VetEntity.css';
import ImageCard from './../../Components/ImageCard';
import InteractiveMap from './../../Components/InteractiveMap';
import Share from './../../Components/Share';

export class VetEntity extends Component{

    componentWillMount(){
        this.props.fetchVetEntity(this.props.match.params.vetId);
    }

    render(){
        /*
        populate side and bottom bar with nearby shelters, uses vetpetlink to get nearby pets.
        */
        let info = this.props.vetInfo;
        let image = {
            background: 'linear-gradient(to right, #020202 0%,#020202 10%,  transparent 50%, transparent), url('+ info.image_url +') no-repeat center',
            backgroundSize: 'cover, 100% auto'
        };
        const close_shelters = [];
        if (info.close_shelters !== undefined)
            for (var i = 0; i < Math.min(2, info.close_shelters.length); i++) {
                close_shelters.push(<ImageCard id={info.close_shelters[i]} type={'shelter'} key={info.close_shelters[i]}/>);
            }

        return (
            <div className="vetEntity listingPage">     
                <div className="jumbotron p-3 p-md-5 text-white rounded bg-dark" style={image}>
                    <div className ="jumbotron_wrapper container">
                        <div className="col-md-6 px-0"> 
                            <h1 className="display-4 font-italic text-white">{info.name}</h1>
                            <p className="lead my-3 text-white">Location: {info.city}</p>
                            <Share type='vet' id={this.props.match.params.vetId} />
                        </div>
                    </div>
                </div>
                <main role="main" className="container">
                    <div className="row">
                        <div className="col-md-8 blog-main">
                            <div className="row mb-2">
                                <div className="col-md-12">
                                    <div className="card flex-md-row mb-4 box-shadow">
                                        <div className="card-body d-flex flex-column align-items-start">
                                            <a href={info.website_url}>
                                                <h2 className="d-inline-block mb-2 text-black"><b>{info.name}</b></h2>
                                            </a>
                                            {info.address1 !== null ? <div className="mb-1 text-black"><b>Address:</b> {info.address1}</div> : null}
                                            <div className="mb-1 text-black"><b>City: </b>{info.city}</div>
                                            <div className="mb-1 text-black"><b>State: </b>{info.state}</div>
                                            <div className="mb-1 text-black"><b>Phone: </b> {info.display_phone}</div>
                                            <div className="mb-1 text-black"><b>Rating: </b></div>
                                            <RatingStar rating={info.rating}/>
                                            <br/>
                                            {info.name !== undefined ?
                                            <InteractiveMap latitude={this.props.vetInfo.latitude} longitude={this.props.vetInfo.longitude} name={info.name} />
                                            : null}     
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <aside className="col-md-4 blog-sidebar">
                            <h3 className="d-inline-block mb-2 text-black">Nearby Shelters:</h3>
                            {close_shelters}
                        </aside>

                    </div>

                    <div className="container">
                        <h3 className="d-inline-block mb-2 text-black">Nearby Pets:</h3>
                        {info.name !== undefined ? 
                            <VetPetLink id={info.close_shelters[0]} /> : null
                        }
                    </div>
                </main>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    vetInfo: state.vetInfo.items
})

export default connect(mapStateToProps, {fetchVetEntity})(VetEntity)
