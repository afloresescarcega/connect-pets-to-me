import React, {Component} from 'react';
import './../../CSS/Carousel.css';
import {Link} from 'react-router-dom'
import petsimg from '../../Assets/pet_splash.jpg'
import vetsimg from '../../Assets/vet_splash.jpg'
import shelterimg from '../../Assets/shelter_splash.jpg'

class Carousel extends Component {
  render(){

    let image1 = {
      backgroundImage: `url(${petsimg})`
    }

    let image2 = {
      backgroundImage: `url(${vetsimg})`
    }
    let image3 = {
      backgroundImage: `url(${shelterimg})`
    }

    return(
      <div className = "splash">
        <div id="frontPageSlider" className="carousel slide" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#frontPageSlider" data-slide-to="0" className="active"></li>
            <li data-target="#frontPageSlider" data-slide-to="1"></li>
            <li data-target="#frontPageSlider" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner" role="listbox">
            <div className="carousel-item active" style={image1}>
              <div className="carousel-caption" id="carouselLink1">
                <Link to="/FindPets/1">
                  <h2>Adopt a pet today</h2>
                </Link>
                <p>Saving a life could change yours</p>
              </div>
            </div>
            <div className="carousel-item" style={image2}>
              <div className="carousel-caption" id="carouselLink2">

                <Link to="/FindVets/1">
                  <h2>Find a veterinarian nearby</h2>
                </Link>
                <p>Vaccinations and regular check ups help keep your pet healthy</p>
              </div>
            </div>
            <div className="carousel-item" style={image3}>
              <div className="carousel-caption" id="carouselLink3">
              <Link to="/FindShelters/1" >
                <h2>Visit a shelter</h2>
              </Link>
                <p>Find out more about your local shelters</p>
              </div>
            </div>
          </div>
          <a className="carousel-control-prev" href="#frontPageSlider" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#frontPageSlider" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div> 
      </div>
      )
  }
}

export default Carousel;
