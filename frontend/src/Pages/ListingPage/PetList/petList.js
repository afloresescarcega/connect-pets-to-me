import React, { Component } from 'react';
import PetItems from './../../../Components/Model/Pets/PetItems';
import PageBar from './../../../Components/PageBar';
import PageHeader, {HeaderItem} from './../../../Components/PageHeader'
import { fetchData } from './../../../Actions/dataModelAction'
import {connect} from 'react-redux';
import FilterMenu from './../../../Components/ExtraFilters'

export class petList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "current_page": 1,
    };
  }
  
  // get pet list from database
  componentWillMount() {
    this.props.fetchData('pet', this.props.match.params.pageNum, this.props.location.search);
    this.setState({current_page: this.props.match.params.pageNum});
  }
  // check if page number has changed, or query string has changed
  componentDidUpdate(prevProps){
    if (prevProps.match.params.pageNum !== this.props.match.params.pageNum || prevProps.location.search !== this.props.location.search){
      this.setState({current_page: this.props.match.params.pageNum});
      this.props.fetchData('pet', this.props.match.params.pageNum, this.props.location.search); 
    }
  }
  
  render() {
    let pageNum = this.props.match.params.pageNum;
    let petItems = this.props.pets.map(pet => {
      return(
        <PetItems petData = {pet} key={pet.id}/>
      );
    });
   
    return (
      <div className="album py-5 bg-light listingPage">
        <div className="container fillPage">
          <PageHeader heading="Pets" domain="/FindPets/" query={this.props.location.search}>
            <HeaderItem type="state" domain="/FindPets/" query={this.props.location.search}/>
            <HeaderItem type="city" domain="/FindPets/" query={this.props.location.search}/>
            <HeaderItem type="animal" domain="/FindPets/" query={this.props.location.search}/>
          </PageHeader>
          <br />
          <div className="btn-group spaced" role="group">
            <FilterMenu domain="/FindPets/" query={this.props.location.search} type="sex"/> 
            <FilterMenu domain="/FindPets/" query={this.props.location.search} type="age"/> 
            <FilterMenu domain="/FindPets/" query={this.props.location.search} type="size"/>
          </div>
          <br />
          <br />
          <div className="row">
            {petItems}
          </div>
        </div>
        <br />
        <PageBar domain="/FindPets/" pageNum={pageNum} totalPages={this.props.total_page} query={this.props.location.search}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  pets: state.pets.pet_items,
  total_page: state.pets.pet_last_page,
  current_page: state.pets.pet_current_page
});

export default connect(mapStateToProps, { fetchData })(petList)