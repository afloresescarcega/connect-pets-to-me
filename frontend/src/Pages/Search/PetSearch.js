import React, { Component } from 'react';
import PageBar from '../../Components/PageBar'
import axios from 'axios';
import SearchHeader from './SearchHeader';
import SearchResult from './SearchResult';
import { HOST } from '../../Actions/type';

export default class PetSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      petData: [],
      totalItems: 1,
      q: this.props.location.search,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.match.params.pageNum !== prevProps.match.params.pageNum || this.props.location.search !== prevProps.location.search) {
      this.getPets();
    }
  }

  searchUpdate(query){
    this.forceUpdate();
  }
  componentWillMount() {
    this.getPets();
  }

  getPets() {
    let url = "http://"+HOST+":5000/api/pet/search".concat(this.props.location.search).concat("&page=").concat(this.props.match.params.pageNum).concat("&size=9");
    axios.get(
      url, {
        headers: { 
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
        },
        responseType: 'json',
    }).then(res =>(this.setState({petData: res.data["hits"], totalItems: res.data.total})))
  }

  render() {
    let petItems = this.state.petData.map(pet => 
      <SearchResult url="/Pets/PetEntity/" info={pet} key={pet._id}/>
    )
    return (
      <div>
        <div className="album py-5 bg-light listingPage">
          <div className="container fillPage">
            <SearchHeader domain="Pets" updateParent={this.searchUpdate.bind(this)}/>
            <br />
            {petItems}
          </div>
          <br />
          <PageBar domain="/SearchPets/" pageNum={this.props.match.params.pageNum} totalPages={Math.ceil(this.state.totalItems / 9)} query={this.props.location.search} />
        </div>
      </div>
    )
  }
}