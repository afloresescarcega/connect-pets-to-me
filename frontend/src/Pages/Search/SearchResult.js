import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import "./../../CSS/SearchResult.css";

export default class SearchResult extends Component{
    constructor(props){
        super(props);
        this.highlighted_text = "";
    }
    
    componentWillMount(){
        // Bold search terms in the results
        var str = "<div>".concat(this.props.info["highlight"]["_all"][0]).replace(/em/gi,"strong").concat("</div>");
        this.highlighted_text = str.replace(/http(s)?/i, ""); // Remove unwaned 'http' text returned by Elastic Search
    }

    render(){
        return (
            <div className = "SearchResult-card">
                <div className="card mb-4 col-md-9 box-shadow">
                    <div className="row no-gutters">
                        <div className="col-auto">
                            {/* Use a default image if missing, otherwise use one provided */}
                            <Link to={this.props.url.concat(this.props.info['_id'])}> <img src={this.props.info["_source"]["image_url"] === undefined ? this.props.info["_source"]["img_url"] : this.props.info["_source"]["image_url"]} onError={(e)=>{e.target.src="https://www.scandichotels.se/Static/img/placeholders/image-placeholder_3x2.svg"}} className="result-img" alt=""/> </Link>
                        </div>
                        <div className="col-md-9">
                            <div className="card-block px-2">
                                <Link to={this.props.url.concat(this.props.info['_id'])}>
                                    <h4 className="card-title">{this.props.info["_source"]["name"]}</h4>
                                </Link>
                                <p className="card-text" dangerouslySetInnerHTML={{ __html: this.highlighted_text }}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
