import React, { Component } from 'react';
import PageBar from '../../Components/PageBar'
import axios from 'axios';
import SearchResult from './SearchResult';
import SearchHeader from './SearchHeader';
import { HOST } from '../../Actions/type';

export default class VetSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      vetData: [],
      totalItems: 1,
      q: this.props.location.search,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.match.params.pageNum !== prevProps.match.params.pageNum || this.props.location.search !== prevProps.location.search) {
      this.getVets();
    }
  }
  searchUpdate(event){
    this.forceUpdate();
  }
  componentWillMount() {
    this.getVets();
  }

  getVets() {
    let url = "http://"+HOST+":5000/api/vet/search".concat(this.props.location.search).concat("&page=").concat(this.props.match.params.pageNum).concat("&size=9");
    axios.get(
      url, {
        headers: { 
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',          
        },
        responseType: 'json',
    }).then(res =>(this.setState({vetData: res.data["hits"], totalItems: res.data.total})))
  }

  render() {
    let vetItems = this.state.vetData.map(vet => 
      <SearchResult url="/Vets/VetEntity/" info={vet} key={vet._id}/>
    )
    return (
      <div>
        <div className="album py-5 bg-light listingPage">
          <div className="container fillPage">
            <SearchHeader domain="Vets" updateParent={this.searchUpdate.bind(this)}/>
            <br />

            {vetItems}
            <br />
          </div>
          <br />
          <PageBar domain="/SearchVets/" pageNum={this.props.match.params.pageNum} totalPages={Math.ceil(this.state.totalItems / 9)} query={this.props.location.search} />
        </div>
      </div>
    )
  }
}