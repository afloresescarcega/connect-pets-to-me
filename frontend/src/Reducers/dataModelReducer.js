import {FETCH_PET_PAGE, FETCH_VET_PAGE, FETCH_SHELTER_PAGE} from './../Actions/type';

const initialState = {
    pet_items: [],
    shelter_items: [],
    vet_items: [],
    item: {},
    pet_current_page: 1,
    pet_last_page: 1,
    vet_current_page: 1,
    vet_last_page: 1, 
    shelter_current_page: 1,
    shelter_last_page: 1,
};

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_PET_PAGE:
            return{
                ...state,
                pet_items: action.data.objects,
                pet_current_page: action.data.page,
                pet_last_page: action.data.total_pages
            };
        case FETCH_VET_PAGE:
            return{
                ...state,
                vet_items: action.data.objects,
                vet_current_page: action.data.page,
                vet_last_page: action.data.total
            };
        case FETCH_SHELTER_PAGE:
            return{
                ...state,
                shelter_items: action.data.objects,
                shelter_current_page: action.data.page,
                shelter_last_page: action.data.total
            };
        default:
            return state;
    }
}
