import expect from 'expect';
import FilterMenu from './../src/Components/ExtraFilters';
import {DropItem} from './../src/Components/PageHeader';
import React from 'react';
import { mount, shallow } from 'enzyme';

describe ("tests ExtraFilters.js", () => {
  it ("Renders without exploding", () => {
    const wrapper = shallow(<FilterMenu type="" query={{}} domain=""/>);
    expect(wrapper.length).toBe(1);
  })

  it ("Renders sex categories", () => {
    const wrapper = shallow(<FilterMenu type="sex" query={{}} domain=""/>);
    expect(wrapper.find('DropItem').length).toBe(1);
  })

  it("Renders a reset button", () => {
    const wrapper = shallow(<FilterMenu type="size" query={{}} domain="" />);
    expect(wrapper.find('Link').length).toBe(1);
  })

  it ("Changes text with a filter selected", () => {
    const wrapper = shallow(<FilterMenu type="sex" query={"?sex=M"} domain="" />);
    expect(wrapper.find('.dropdown-toggle').text()).toBe('Sex: Male');
  })
})