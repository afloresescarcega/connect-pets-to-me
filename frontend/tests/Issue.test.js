import expect from 'expect';
import Issue from './../src/Components/Issue';
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import fetch from 'isomorphic-fetch';

describe("Tests Issue.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<Issue />);  
    expect(wrapper.length).toBe(1);
  });
});
