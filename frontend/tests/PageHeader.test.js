import expect from 'expect';
import PageHeader, {HeaderItem} from './../src/Components/PageHeader';
import {DropItem} from './../src/Components/DropItem'
import React from 'react';
import { mount, shallow } from 'enzyme';

describe("tests PageHeader.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<PageHeader domain=""/>);  
    expect(wrapper.length).toBe(1);
  });
});

describe("tests HeaderItem component", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<HeaderItem domain="" type=""/>);
    expect(wrapper.length).toBe(1);
  })

  it("Does not render city DropItem if no State is selected", () => {
    const wrapper = shallow(<HeaderItem domain="" type="city" query=""/>);
    expect(wrapper.find('DropItem').length).toBe(0);
  })

  it("Renders city DropItem if a State is selected", () => {
    const wrapper= shallow(<HeaderItem domain="" type="city" query="?state=a" />);
    expect(wrapper.find('DropItem').length).toBe(1);
  })
})

describe("tests DropItem component", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<DropItem />);
    expect(wrapper.length).toBe(1);
  })

  it ("Renders all 50 states", () => {
    const wrapper = shallow(<DropItem type="state" domain=""/>);
    expect(wrapper.find('.state-item').length).toBeGreaterThanOrEqual(50);
  })

  it ("Has all of the animal types", () => {
    const wrapper = shallow(<DropItem type="animal" domain="" />);
    expect(wrapper.find('.animal-item').length).toBe(8);
  })
})