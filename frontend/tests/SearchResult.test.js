import expect from 'expect';
import SearchResult from './../src/Pages/Search/SearchResult';
import React from 'react';
import { mount, shallow } from 'enzyme';

let sampleResult = {
_id: "TN801",
_index: "connectpetstome_shelter",
_score: 0.9516685,
_source: {
address1: "2288 Gunbarrel Road",
address2: null,
city: "Chattanooga",
img_url: "https://maps.googleapis.com/maps/api/staticmap?center=35.0459,-85.3093&zoom=12&size=250x250&scale=2&markers=color:blue%7Csize:tiny%7Clabel:S%7C35.0459,-85.3093&key=AIzaSyAVelkQ8ysqaPPt5UHxv_IOHuaYFBpbDiM",
name: "Meow Meow Kitty Rescue",
phone: "2014031036",
state: "TN Tennessee",
type: "animal shelter",
zip: "37424"
},
_type: "shelter",
highlight: {
_all: [
"animal shelter Meow Meow <em>Kitty</em> Rescue 2288 Gunbarrel Road Chattanooga TN Tennessee 37424 2014031036"
]
}}
describe("tests SearchResult.js", () => {
  it("Renders without exploding", () => {
    const wrapper = shallow(<SearchResult url="" info={sampleResult}/>)
    expect(wrapper.length).toBe(1);
  })

  it ("Contains two links", () => {
    const wrapper = shallow(<SearchResult url="" info={sampleResult}/>)
    expect(wrapper.find('Link').length).toBe(2);
  })

  it ("Contains name of search result", () => {
    const wrapper = shallow(<SearchResult url="" info={sampleResult} />)
    expect(wrapper.find('.card-title').text()).toBe("Meow Meow Kitty Rescue");
  })

  it ("Contains image of search result", () => {
    const wrapper = shallow(<SearchResult url="" info={sampleResult} />)
    expect(wrapper.find('img').prop('src')).toBe(sampleResult['_source']['img_url']);
  })
})