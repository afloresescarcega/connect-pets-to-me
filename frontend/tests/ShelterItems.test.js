import expect from 'expect';
import ShelterItems from './../src/Components/Model/Shelters/ShelterItems';
import React from 'react';
import { shallow, mount, render } from 'enzyme';

describe("Tests ShelterItems.js", () => {

  const minProps = {
    shelterData:
        {
            id: "2",
            name: "Austin Wildlife Rescue",
            image: "https://s3-media3.fl.yelpcdn.com/bphoto/yrmDh-ueiSju9B5JwY64fg/ls.jpg",
            location: "Austin",
            rating: "4.5",
            contact: "(512) 472-9453"
        }
  }

  test("Renders without exploding", () => {

    const wrapper = shallow(<ShelterItems {...minProps}/>);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
  });

  test("Two links to its entity page", () => {

    const wrapper = shallow(<ShelterItems {...minProps}/>);
    expect(wrapper.find({ to: '/Shelters/ShelterEntity/'.concat(minProps.shelterData.id) }).length).toEqual(2);
  });

});
