import expect from 'expect';
import {VetEntity} from './../src/Pages/Entities/VetEntity';
import React from 'react';
import { shallow, mount, render } from 'enzyme';

describe("Tests VetEntity.js", () => {

  let mockFetch = jest.fn();
  const minProps = {
    vetInfo: {
      address1: "",
      address2 : "",
      address3 : "",
      city : "Chicago",
      close_shelters : ["IL245", "IL27", "IL537", "IL608", "IL666", "IL751"],
      country : "US",
      display_address : "a",
      display_phone : "(312) 388-5150",
      id : "-GtQ2DCYD5VQ3IOoemDgew",
      image_url : "http : /s3-media2.fl.yelpcdn.com/bphoto/0z619pb-I4Y2KAAXQZTVDQ/o.jpg",
      latitude : 41.9944,
      longitude : -87.6642,
      name : "Cozy Cats and Daily Dogs",
      pets : [],
      phone : "+13123885150",
      rating : 5,
      review_count : 88,
      state : "IL",
      zip_code : "60660",
    },
    fetchVetEntity: {mockFetch},
    match:{params: {vetId:"123"}}
  }

  test("Renders without exploding", () => {
    let mockFetch = jest.fn();
    const wrapper = shallow(<VetEntity {...minProps} fetchVetEntity={mockFetch}/>);

    expect(wrapper.length).toBe(1);
    expect(wrapper).toBeDefined();
    expect(mockFetch).toHaveBeenCalled();
    // expect(mockFetch.mock.calls[0]).toEqual(['data'])
  });

});
