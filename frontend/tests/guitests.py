#!/usr/bin/env python
"""
Test run for the gui of connectpetsto.me

"""
from sys import platform
import unittest
import requests
from unittest import TestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys


# options = webdriver.ChromeOptions()
# options.add_argument('--headless')
if platform == "linux" or platform == "linux2":
    driver = webdriver.Chrome('./drivers/chromedriver_linux64')
elif platform == "darwin": # macOS
    driver=webdriver.Chrome('./drivers/chromedriver_mac64')
elif platform == "win32":
    driver=webdriver.Chrome('./drivers/chromedriver.exe')

driver.set_page_load_timeout(30)
driver.get("http://localhost")
driver.maximize_window()
driver.implicitly_wait(5)

class MyUnitTests (TestCase):
    def test2(self):
        """
        Iterate through items in navbar to see if clickable
        """
        elements = driver.find_elements_by_class_name('nav-link')
        if not elements:
            self.fail("No nav bar elements found")
        for ele in elements:
            try:
                ele.click()
                pass
            except Exception as e:
                print(e)
                self.fail("This nav bar element is not clickable")

    def test3(self):
        """
        Test to see if the back to home button works from FindPets
        """
        driver.get("http://localhost/FindPets/1")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test4(self):
        """
        Test to see if the back to home button works from FindVets
        """
        driver.get("http://localhost/FindVets/1")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test5(self):
        """
        Test to see if the back to home button works from FindShelters
        """
        driver.get("http://localhost/FindShelters/1")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test6(self):
        """
        Test to see if the back to home button works from About
        """
        driver.get("http://localhost/About")
        try:
            driver.find_element_by_class_name("navbar-brand").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test7(self):
        """
        Test to see that every Pet card image on "Find Pets" page
        links to another page
        """
        driver.get("http://localhost/FindPets/1")
        elements = driver.find_elements_by_class_name('petimg')

        if not elements:
            self.fail("No pet img elements found")
        for ele in elements:
            # driver.get("http://localhost/FindPets")
            try:
                link = ele.find_element_by_tag_name("a").get_attribute("href")
                if link[0:5] == "https":
                    continue
                req = requests.get(link)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link))
            except Exception as e:
                print(e, )
                self.fail("Something went wrong with testing to see if this " \
                        "pet img page was a broken link: " + str(link))

    def test8(self):
        """
        Test to see that every Vet card image on "Find Vets" page
        links to another page
        """
        driver.get("http://localhost/FindVets/1")
        elements = driver.find_elements_by_class_name('vetimg')
        if not elements:
            self.fail("No vet img elements found")
        for ele in elements:
            # driver.get("http://localhost/FindVets")
            try:
                link = ele.find_element_by_tag_name("a").get_attribute("href")
                if link[0:5] == "https":
                    continue
                req = requests.get(link)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link))
            except Exception as e:
                print(e, )
                self.fail("Something went wrong with testing to see if this " \
                        "vet img page was a broken link: " + str(link))

    def test10(self):
        """
        Check for broken links in home page
        """
        driver.get("http://localhost")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                addr = link.get_attribute('href')
                # skip links with https link because we need a certificate
                if addr[0:5] == "https":
                    continue
                req = requests.get(addr)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(addr))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if home page " \
                        "had broken links: " + str(link.get_attribute('href')))

    def test11(self):
        """
        Check for broken links in FindPets page
        """
        driver.get("http://localhost/FindPets/1")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                addr = link.get_attribute('href')
                # skip links with https link because we need a certificate
                if addr[0:5] == "https" or addr == "":
                    continue
                req = requests.get(addr)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(addr))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if FindPets " \
                        "page had broken links: " + str(link.get_attribute('href')))


    def test12(self):
        """
        Check for broken links in FindVets page
        """
        driver.get("http://localhost/FindVets/1")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                # skip links with https link because we need a certificate
                addr = link.get_attribute('href')
                if addr[0:5] == "https" or addr == "":
                    continue
                req = requests.get(addr )
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(addr))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if FindVets " \
                        "page had broken links: " + str(link.get_attribute('href')))

    def test13(self):
        """
        Check for broken links in FindShelters page
        """
        driver.get("http://localhost/FindShelters/1")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                addr = link.get_attribute('href')
                # skip links with https link because we need a certificate
                if addr[0:5] == "https" or addr == "":
                    continue
                req = requests.get(addr)
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(addr))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if FindShelters " \
                        "page had broken links: " + str(link.get_attribute('href')))

    def test14(self):
        """
        Check for broken links in About page
        """
        driver.get("http://localhost/About")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        # links_checks = 0
        for link in links:
            # links_checks += 1
            try:
                # skip links with https link because we need a certificate
                if link.get_attribute('href')[0:5] == "https":
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link.get_attribute('href')))
            except Exception as e:
                print(e, )
                self.fail("Something went wrong with testing to see if About " \
                    "page had broken links: " + str(link.get_attribute('href')))
    
    def test15(self):
        """
        Check for borken links in an example Search page
        """
        driver.get("http://localhost/Search/1?q=dogs")
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags were found")
        for link in links:
            try:
                # Skip all links that need certificate
                if link.get_attribute('href')[0:5] == "https":
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link" + str(link.get_attribute('href')))
            except NoSuchElementException:
                self.fail("Something went wrong with testing to seee if About " \
                    "page had broken links: " + str(link.get_attribute('href')))
    
    def test16(self):
        """
        Enter something into the search field, hit enter
        and check the url to see if the url is correct 
        """
        driver.get("http://localhost/")
        search_bar = driver.find_element_by_id("search-input")
        if not search_bar:
            self.fail("There is no search bar!")
        
        try:
            search_bar.send_keys("Dogs")
            search_bar.send_keys(Keys.ENTER)
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/Search/1?q=Dogs":
                self.fail("The search field does not match expected. Actual: " + str(driver.current_url)) 

    def test17(self):
        """
        Enter something into the search field, click search!
        and check the url to see if the url is correct 
        """
        driver.get("http://localhost/")
        search_bar = driver.find_element_by_id("search-input")
        searchButton = driver.find_element_by_class_name("btn")
        if not search_bar:
            self.fail("There is no search bar!")
        
        try:
            search_bar.send_keys("Dogs")
            searchButton.click()
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/Search/1?q=Dogs":
                self.fail("The search field does not match expected. Actual: " + str(driver.current_url)) 

    def test18(self):
        """
        Go to (petlist) page and enter something into (petlist) search field, hit enter,
        and check the url to see if the url is correct
        """
        driver.get("http://localhost/FindPets/1")
        search_bars = driver.find_elements_by_class_name("form-control")
        if len(search_bars) == 0:
            self.fail("There is no search bars!")
        
        try:
            search_bars[1].send_keys("Dogs")
            search_bars[1].send_keys(Keys.ENTER)
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/SearchPets/1?q=Dogs":
                self.fail("The pets search field does not match expected. Actual: " + str(driver.current_url))

    def test19(self):
        """
        Go to (vetlist) page and enter something into (vetlist) search field, hit enter,
        and check the url to see if the url is correct
        """
        driver.get("http://localhost/FindVets/1")
        search_bars = driver.find_elements_by_class_name("form-control")
        if len(search_bars) == 0:
            self.fail("There is no search bars!")
        
        try:
            search_bars[1].send_keys("Dogs")
            search_bars[1].send_keys(Keys.ENTER)
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/SearchVets/1?q=Dogs":
                self.fail("The vets search field does not match expected. Actual: " + str(driver.current_url))

    def test20(self):
        """
        Go to (shelterlist) page and enter something into (shelterlist) search field, hit enter,
        and check the url to see if the url is correct
        """
        driver.get("http://localhost/FindShelters/1")
        search_bars = driver.find_elements_by_class_name("form-control")
        if len(search_bars) == 0:
            self.fail("There is no search bars!")
        
        try:
            search_bars[1].send_keys("Dogs")
            search_bars[1].send_keys(Keys.ENTER)
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/SearchShelters/1?q=Dogs":
                self.fail("The shelter search field does not match expected. Actual: " + str(driver.current_url))
    
    def test21(self):
        """
        Go to (petlists) page and enter something into (petlists) search field, hit enter,
        and check the url to see if the url is correct
        """
        driver.get("http://localhost/FindPets/1")
        search_bars = driver.find_elements_by_class_name("form-control")
        if len(search_bars) == 0:
            self.fail("There is no search bars!")
        
        try:
            search_bars[1].send_keys("Kitty")
            search_bars[1].send_keys(Keys.ENTER)
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/SearchPets/1?q=Kitty":
                self.fail("The shelter search field does not match expected. Actual: " + str(driver.current_url))

    def test22(self):
        """
        Go to (vetlists) page and enter something into (vetlists) search field, hit enter,
        and check the url to see if the url is correct
        """
        driver.get("http://localhost/FindVets/1")
        search_bars = driver.find_elements_by_class_name("form-control")
        if len(search_bars) == 0:
            self.fail("There is no search bars!")
        
        try:
            search_bars[1].send_keys("Kitty")
            search_bars[1].send_keys(Keys.ENTER)
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/SearchVets/1?q=Kitty":
                self.fail("The shelter search field does not match expected. Actual: " + str(driver.current_url))
    
    def test23(self):
        """
        Go to (shelterlist) page and enter something into (shelterlist) search field, hit enter,
        and check the url to see if the url is correct
        """
        driver.get("http://localhost/FindShelters/1")
        search_bars = driver.find_elements_by_class_name("form-control")
        if len(search_bars) == 0:
            self.fail("There is no search bars!")
        
        try:
            search_bars[1].send_keys("Kitty")
            search_bars[1].send_keys(Keys.ENTER)
        except Exception as e:
            self.fail("Something went wrong with entering into the search field")
        else:
            if driver.current_url != "http://localhost/SearchShelters/1?q=Kitty":
                self.fail("The shelter search field does not match expected. Actual: " + str(driver.current_url))

        


                
        
        

    @classmethod
    def tearDownClass(cls):
        driver.close()
        driver.quit()


if __name__ == "__main__":
    unittest.main()
